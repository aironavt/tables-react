/**
 * Преобразует unix time в 04.11.1998
 * @param unixTime
 * @returns {*}
 */
export function formatDate(unixTime) {

  if(Number(unixTime) < 1) {
    return '';
  }

  const date = new Date(unixTime * 1000);

  //Формируем дату
  let newValue = [date.getDate(), date.getMonth() + 1, date.getFullYear()];

  //Добавляем ведущий ноль
  newValue = newValue.map((item) => item < 10 ? `0${item}` : item);

  return newValue.join('.');
}

/**
 * Преобразует 04.11.98 или 25.02.2016 в unix time
 * @param string
 * @returns {number}
 */
export function parseDate(string) {
  let [day, month, year] = string.split(/[^\d]/);
  let result = new Date(`${month}.${day}.${year}`);
  return result /= 1000;
}

export function formatBoolean(value) {
  return value ? 'Да' : 'Нет';
}
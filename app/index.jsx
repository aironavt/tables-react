import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducer, {actions} from './reducer';
import Filter from './components/filter';
import Table from './components/table';
import AddEntry from './components/add-entry';
import './styles/style.scss';
import {connect} from 'react-redux';

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
const names = {
  'name': {
    name: 'Имя Фамилия',
    placeholder: 'Sigmund Jähn',
  },
  'date': {
    name: 'Дата первого полета',
    type: 'date',
    placeholder: '26.08.1978',
  },
  'days': {
    name: 'Количество дней',
    type: 'number',
    placeholder: '7',
  },
  'mission': {
    name: 'Название мисии',
    placeholder: 'Sojus 31 / Sojus 29',
  },
  'isMultiple': {
    name: 'Повторный полёт',
    className: 'text-align_right',
    type: 'boolean',
  },
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.toggleAddition = this.toggleAddition.bind(this);
  }

  toggleAddition() {
    this.props.toggleAddition();
  }

  render() {
    const toggleAdditionText = this.props.showAddition ? 'Закрыть' : 'Добавить космонавта';

    return (
      <div className="content">
        <div className="content__top-decorator"/>
        <h1 className="content__title">Список космонавтов</h1>
        <Filter names={names}
                items={this.props.items}
                filter={this.props.filter}
                setFilter={this.props.setFilter}/>
        <div className="horizontal-line"/>
        <div className="title-bar">
          <h2 className="title-bar__text">Список космонавтов</h2>
          <div className="title-bar__button">
            <input type="button"
                   className="button"
                   value={toggleAdditionText}
                   onClick={this.toggleAddition}/>
          </div>
        </div>
        {this.props.showAddition && <AddEntry names={names}
                                              addItem={this.props.addItem}/>}
        <Table names={names} {...this.props}/>
        <div className="content__bottom-decorator"/>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {...state};
}

export const AppConnect = connect(mapStateToProps, actions)(App);

ReactDOM.render(<Provider store={store}><AppConnect/></Provider>, document.getElementById('app'));
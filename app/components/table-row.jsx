import React from 'react';
import humps from 'humps';
import {formatDate, formatBoolean} from 'tools';

export default class TableRow extends React.Component {
  render() {
    let cells = [];
    const names = this.props.names;

    for(let cellKey in names) {
      if(!names.hasOwnProperty(cellKey)) {
        continue;
      }

      const cell = names[cellKey];
      let className = cell.className || '';
      let type = cell.type || 'string';

      const kebabClassName = humps.decamelize(cellKey, { separator: '-' });

      className = `${className} table__cell table__cell_w-${kebabClassName}`.trim();

      let value = this.props.item[cellKey];

      if(type === 'boolean') {
        value = formatBoolean(value);
      }

      if(type === 'date') {
        value = formatDate(value);
      }

      cells.push(<div className={`${className}`} key={cellKey}>{value}</div>);
    }

    return (
      <div className="table_row">
        {cells}
        <div className="table__cell table__cell-delete"
             onClick={() => this.props.deleteItem(this.props.item.name)}>
          <div className="table__button-delete" title="Удалить">&#215;</div>
        </div>
      </div>
    );
  }
}

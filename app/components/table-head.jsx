import React from 'react';
import humps from 'humps';

export default class TableHead extends React.Component {
  constructor(props) {
    super(props);
    this.handleSortItem = this.handleSortItem.bind(this);
  }

  handleSortItem(sortType) {
    let sortOrder = 'asc';
    if(this.props.sortType === sortType) {
      sortOrder = this.props.sortOrder === 'asc' ? 'desc' : 'asc';
    }
    this.props.sortItems(sortType, sortOrder);
  }

  render() {
    let cells = [];
    const names = this.props.names;

    for(let cellKey in names) {
      if (!names.hasOwnProperty(cellKey)) {
        continue;
      }

      const cell = names[cellKey];
      let className = cell.className || '';
      let name = cell.name;
      let sortType = '';

      const kebabClassName = humps.decamelize(cellKey, { separator: '-' });
      //Если есть что сортировать, то добавляем в заголовок возможность сортировки
      const sortClass = this.props.count !== 0 ? ' table__cell_sort' : '';
      //Собираем все css классы
      className = `${className} table__cell table__cell_w-${kebabClassName}${sortClass}`.trim();

      //Выводим направление сортировки
      if(cellKey === this.props.sortType && this.props.count !== 0) {
        //Определяем направление сортировки
        const sortOrder = this.props.sortOrder ? `table__sort-type_${this.props.sortOrder}` : '';
        sortType = <span className={`table__sort-type ${sortOrder}`}/>;
      }

      cells.push(
        <div className={`${className}`}
             key={cellKey}
             onClick={() => {
               this.props.count !== 0 && this.handleSortItem(cellKey)
             }}>
          {name}
          {sortType}
        </div>
      );
    }

    return (<div className="table__head">{cells}</div>);
  }
}

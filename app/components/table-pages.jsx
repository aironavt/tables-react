import React from 'react';

export default class TablePages extends React.Component {
  constructor(props) {
    super(props);
    this.handlePrevPages = this.handlePrevPages.bind(this);
    this.handleNextPages = this.handleNextPages.bind(this);
  }

  isDisabledPrev() {
    return this.props.count < 1 || this.props.currentPage === 1;
  }

  isDisabledNext() {
    return this.props.currentPage >= Math.ceil(this.props.count / this.props.pageSize);
  }

  handlePrevPages() {
    this.props.prevPage();
  }

  handleNextPages() {
    this.props.nextPage();
  }

  render() {
    if(this.props.count === 0) {
      return null;
    }

    const startingPosition = (this.props.currentPage - 1) * this.props.pageSize + 1;
    const endPosition = Math.min(startingPosition + this.props.pageSize - 1, this.props.count);

    return (
      <div className="table__pages">
        <div className="table__pages-count">
          Показано:{' '}
          <span className="table__pages-count-value">{startingPosition}</span> — {' '}
          <span className="table__pages-count-value">{endPosition}</span> из {' '}
          <span className="table__pages-count-value">{this.props.count}</span>
        </div>
        <div className="table__pages-buttons">
          <input type="button"
                 className="button table__pages-button"
                 value="&larr; Предыдущая"
                 title="Предыдущая страница"
                 disabled={this.isDisabledPrev()}
                 onClick={this.handlePrevPages}
          />
          <input type="button"
                 className="button table__pages-button"
                 value="Следующая &rarr;"
                 title="Следующая страница"
                 disabled={this.isDisabledNext()}
                 onClick={this.handleNextPages}
          />
        </div>
      </div>
    );
  }
}

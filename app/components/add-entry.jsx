import React from 'react';
import humps from 'humps';
import {parseDate} from 'tools';

export default class AddEntry extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.inputs = {};
  }

  handleSubmit(e) {
    e.preventDefault();
    let fields = {};

    for(let fieldKey in this.props.names) {
      if (!this.props.names.hasOwnProperty(fieldKey)) {
        continue;
      }

      const input = this.inputs[fieldKey];
      const nameProps = this.props.names[fieldKey];
      let value = '';
      let type = nameProps.type ? nameProps.type : 'string';

      if(input.type === 'checkbox') {
        value = input.checked;
      } else if(type === 'date') {
        value = parseDate(input.value);
      } else if(type === 'number') {
        value = parseInt(input.value);
      } else {
        value = input.value;
      }

      fields[fieldKey] = value;
    }

    this.props.addItem(fields);
  }

  render() {
    let fields = [];
    const names = this.props.names;

    for(let fieldKey in names) {
      if (!names.hasOwnProperty(fieldKey)) {
        continue;
      }

      const field = names[fieldKey];
      let className = field.className || '';
      let name = field.name;
      let type = field.type || 'string';

      const kebabClassName = humps.decamelize(fieldKey, { separator: '-' });

      className = `${className} table__cell table__cell_w-${kebabClassName}`.trim();

      const isFieldCheckbox = type === 'boolean' ? ' addition__field_checkbox' : '';
      let input = null;

      if(isFieldCheckbox) {
        input = <input type="checkbox"
                       id={`addition-${kebabClassName}`}
                       className="checkbox"
                       ref={(input) => this.inputs[fieldKey] = input}/>;
      } else {
        input = <input type="text"
                       id={`addition-${kebabClassName}`}
                       className="text"
                       required={true}
                       placeholder={field.placeholder}
                       ref={(input) => this.inputs[fieldKey] = input}/>;
      }

      fields.push(
        <div className={`addition__field addition__field_w-${kebabClassName}${isFieldCheckbox}`} key={fieldKey}>
          <label htmlFor={`addition-${kebabClassName}`} className="addition_label">{name}</label>
          {input}
        </div>
      );
    }

    return (
      <form className="addition content__addition" onSubmit={this.handleSubmit}>
        {fields}
        <div className="addition__field addition__field_button">
          <input type="submit" className="button button_submit" value="Добавить"/>
        </div>
      </form>
    );
  }
}
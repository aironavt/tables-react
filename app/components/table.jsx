import React from 'react';
import {parseDate} from 'tools';
import TableHead from './table-head';
import TableBody from './table-body';
import TablePages from './table-pages';

export default class Table extends React.Component {
  render() {
    const {currentPage, pageSize, sortType, sortOrder, names, filter} = this.props;
    const currentOffset = pageSize * (currentPage - 1);
    let isFiltered = false;

    const filterItems = this.props.items.filter((item) => {
      let result = true;

      for(let fieldKey in names) {
        if(!names.hasOwnProperty(fieldKey) || typeof filter[fieldKey] === 'undefined' ||
          filter[fieldKey] === '' || filter[fieldKey] === false ||
          typeof item[fieldKey] === 'undefined') {
          continue;
        }

        isFiltered = true;

        let type = 'string';
        let fieldValue = filter[fieldKey];
        const field = names[fieldKey];

        if(typeof field === 'object') {
          type = field.type || type;
        }

        if(type === 'number') {
          fieldValue = parseInt(fieldValue);
        }

        if(type === 'date') {
          fieldValue = parseDate(fieldValue);
        }

        if(type === 'boolean' || typeof fieldValue === 'boolean') {
          fieldValue = !!fieldValue;
        }

        if(type !== 'string') {
          if(item[fieldKey] !== fieldValue) {
            result = false;
            break;
          }
        } else {
          if(item[fieldKey].toLowerCase().indexOf(fieldValue.toLowerCase()) === -1) {
            result = false;
            break;
          }
        }
      }

      return result;
    });

    const sortItems = filterItems.sort((a, b) => {
      if(a[sortType] === b[sortType]) return 0;

      if(sortOrder === 'asc') {
        return a[sortType] > b[sortType] ? 1 : -1;
      } else {
        return a[sortType] > b[sortType] ? -1 : 1;
      }
    });

    const sliceItems = sortItems.slice(currentOffset, currentOffset + pageSize);

    return (
      <div className="table content__table">
        <TableHead names={names}
                   count={filterItems.length}
                   sortType={sortType}
                   sortOrder={sortOrder}
                   sortItems={this.props.sortItems}/>
        <TableBody names={names}
                   rows={sliceItems}
                   isFiltered={isFiltered}
                   deleteItem={this.props.deleteItem}/>
        <TablePages count={filterItems.length}
                    currentPage={currentPage}
                    pageSize={pageSize}
                    prevPage={this.props.prevPage}
                    nextPage={this.props.nextPage}/>
      </div>
    );
  }
}

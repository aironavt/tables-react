import React from 'react';
import TableRow from './table-row';

export default class TableBody extends React.Component {
  render() {
    let rows = null;

    if(this.props.rows.length !== 0) {
      rows = this.props.rows.map(row =>(
        <TableRow item={row} key={row.name + row.date}
                  names={this.props.names}
                  deleteItem={this.props.deleteItem}/>
      ));
    } else {
      rows = <div className="table__empty-body">
        {this.props.isFiltered ? 'Не найдено ни одного космонавта' : 'Нет ни одного космонавта'}
        </div>;
    }

    return (
      <div className="table__body">{rows}</div>
    );
  }
}

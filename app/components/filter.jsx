import React from 'react';
import humps from 'humps';
import {formatDate} from 'tools';

export default class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.inputs = {};
  }

  getValues(props) {
    return this.props.items.reduce((placeholder, item) => {
      return placeholder.concat(item[props]);
    }, []).filter((item) => !!item);
  }

  handleSubmit(e) {
    e.preventDefault();
    let fields = {};

    for(let fieldKey in this.props.names) {
      if (!this.props.names.hasOwnProperty(fieldKey)) {
        continue;
      }

      let value = '';

      if(this.inputs[fieldKey].type === 'checkbox') {
        value = this.inputs[fieldKey].checked;
      } else {
        value = this.inputs[fieldKey].value;
      }

      fields[fieldKey] = value;
    }

    this.props.setFilter(fields);
  }

  render() {
    let fields = [];
    const names = this.props.names;

    for(let fieldKey in names) {
      if (!names.hasOwnProperty(fieldKey)) {
        continue;
      }

      const field = names[fieldKey];
      let className = field.className || '';
      let name = field.name;
      let type = field.type || 'string';

      const kebabClassName = humps.decamelize(fieldKey, { separator: '-' });

      className = `${className} table__cell table__cell_w-${kebabClassName}`.trim();

      const isFieldCheckbox = type === 'boolean' ? ' filter__field_checkbox' : '';
      let input = null;

      if(isFieldCheckbox) {
        input = <input type="checkbox"
                       id={`filter-${kebabClassName}`}
                       className="checkbox"
                       ref={(input) => this.inputs[fieldKey] = input}
                       disabled={this.props.items.length < 1}/>;
      } else {
        let values = this.getValues(fieldKey);
        let placeholder = field.placeholder;

        //Если есть что выводить, для булевых значений не нужна подсказка
        if(values.length !== 0 && type !== 'boolean') {
          //Если число то выводим диапозон значений
          if(typeof values[0] === 'number') {
            let valueMin = Math.min(...values);
            let valueMax = Math.max(...values);

            //Если дата, то фарматируем её
            if(type === 'date') {
              valueMin = formatDate(valueMin);
              valueMax = formatDate(valueMax);
            }

            //Не выводим диапозон с дублирующемися значениями
            placeholder = valueMin !== valueMax ?
              `${valueMin} — ${valueMax}` : valueMin;
          } else {
            placeholder = values[0];
          }
        }

        input = <input type="text"
                       id={`filter-${kebabClassName}`}
                       className="text"
                       placeholder={placeholder}
                       ref={(input) => this.inputs[fieldKey] = input}
                       disabled={this.props.items.length < 1}/>;
      }

      fields.push(
        <div className={`filter__field filter__field_w-${kebabClassName}${isFieldCheckbox}`} key={fieldKey}>
          <label htmlFor={`filter-${kebabClassName}`} className="filter_label">{name}</label>
          {input}
        </div>
      );
    }

    return (
      <form className="filter content__filter" onSubmit={this.handleSubmit}>
        {fields}
        <div className="filter__field filter__field_button">
          <input type="submit"
                 className="button button_submit"
                 value="Паказать"
                 disabled={this.props.items.length < 1}/>
        </div>
      </form>
    );
  }
}

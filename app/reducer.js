const INITIAL_STATE = {
  items: [
    { "name": "Sigmund Jähn", "date": 272926800, "days": 7, "mission": "Sojus 31 / Sojus 29", "isMultiple": false },
    { "name": "Ulf Merbold", "date": 438814800, "days": 10, "mission": "STS-9", "isMultiple": true },
    { "name": "Reinhard Furrer", "date": 499467600, "days": 7, "mission": "STS-61-A (D1)", "isMultiple": false },
    { "name": "Ernst Messerschmid", "date": 499467600, "days": 7, "mission": "STS-61-A (D1)", "isMultiple": false },
    { "name": "Klaus-Dietrich Flade", "date": 700779600, "days": 7, "mission": "Sojus TM-14 / Sojus TM-13", "isMultiple": false },
    { "name": "Hans Schlegel", "date": 735768000, "days": 9, "mission": "STS-55 (D2)", "isMultiple": true },
    { "name": "Ulrich Walter", "date": 735768000, "days": 9, "mission": "STS-55 (D2)", "isMultiple": false },
    { "name": "Thomas Reiter", "date": 810072000, "days": 179, "mission": "Sojus TM-22 / Euromir 95", "isMultiple": true },
    { "name": "Reinhold Ewald", "date": 855522000, "days": 19, "mission": "Sojus TM-25 / Sojus TM-24", "isMultiple": false },
    { "name": "Gerhard Thiele", "date": 950216400, "days": 11, "mission": "STS-99", "isMultiple": false },
    { "name": "Alexander Gerst", "date": 1401224400, "days": 165, "mission": "Sojus TMA-13M / ISS-Expedition 40 /ISS-Expedition 41", "isMultiple": false }
  ],
  pageSize: 5,
  currentPage: 1,
  sortType: '',
  sortOrder: 'asc',
  filter: {},
  showAddition: false,
};

function toggleAddition(state) {
  return {...state, showAddition: !state.showAddition};
}

function addItem(state, fields) {
  return {...state, showAddition: false, items: [...state.items, fields]};
}

function deleteItem(state, entry) {
  //Переходим на предыдущую страницу если не хватает записей
  const currentPage = state.currentPage > Math.ceil((state.items.length - 1) / state.pageSize) ?
    Math.max(state.currentPage - 1, 1) : state.currentPage;

  return {...state, items: state.items.filter(item => item.name !== entry), currentPage};
}

function sortItems(state, sortType, sortOrder) {
  return {...state, sortType, sortOrder};
}

function setFilter(state, filter) {
  return {...state, currentPage: 1, filter};
}

function prevPage(state) {
  const newPage = state.currentPage > 1 ? state.currentPage - 1 : 1;

  return {...state, currentPage: newPage}
}

function nextPage(state) {
  const pagesCount = Math.ceil(state.items.length / state.pageSize);
  const newPage = state.currentPage < pagesCount ? state.currentPage + 1 : pagesCount;

  return {...state, currentPage: newPage}
}

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'TOGGLE_ADDITION':
      return toggleAddition(state);
    case 'ADD_ITEM':
      return addItem(state, action.fields);
    case 'DELETE_ITEM':
      return deleteItem(state, action.entry);
    case 'SORT_ITEMS':
      return sortItems(state, action.sortType, action.sortOrder);
    case 'SET_FILTER':
      return setFilter(state, action.fields);
    case 'PREV_PAGE':
      return prevPage(state);
    case 'NEXT_PAGE':
      return nextPage(state);
  }
  return state;
}

export const actions = {
  toggleAddition() { return { type: 'TOGGLE_ADDITION' }},
  addItem(fields) { return { type: 'ADD_ITEM', fields }},
  deleteItem(name) { return { type: 'DELETE_ITEM', entry: name }},
  sortItems(sortType, sortOrder) { return { type: 'SORT_ITEMS', sortType, sortOrder}},
  setFilter(fields) { return { type: 'SET_FILTER', fields}},
  prevPage() { return { type: 'PREV_PAGE' }},
  nextPage() { return { type: 'NEXT_PAGE' }},
};